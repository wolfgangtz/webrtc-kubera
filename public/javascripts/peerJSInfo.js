$(document).ready(function() {
    /** PARA TENER EN CUENTA **/
    // Herramientas de peerJS de interes:
    //
    //util.browser --> el navegador actual, puede dar como resultado  'Firefox',
    // 'Chrome', 'Unsupported', or 'Supported' (unknown WebRTC-compatible browser)
    //forma de usarse:
    // if (util.browser === 'Firefox') { /* OK to peer with Firefox peers. */ }
    //
    //util..supports --> Un hash de características WebRTC asignadas a booleanos 
    //que corresponden a si la función es compatible con el navegador actual.
    //posibilidades: 
    //.audioVideo = true si el navegador actual admite flujos 
    //  de medios y PeerConnection.
    // .data = true si el navegador actual admite DataChannel y PeerConnection
    // .binary = true si el navegador actual admite DataChannels binario.
    // .reliable = true si el navegador actual admite DataChannels confiable.
    //forma de usarse: 
    // if (util.supports.data) { /* OK to start a data connection. */ }
    //DataConnection = Envuelve el DataChannel de WebRTC. Para obtener uno, 
    //  use peer.connect o escuche el evento connect.
    //
    //MediaConnection = Envuelve los flujos de medios de WebRTC. Para 
    //  obtener uno, use peer.call o escuche el evento de llamada.
    var peer; // Donde se guarda la instancia de la conexion con peer
    var videoStreaming; // Guardamos el streaming de video local.
    var audioStreaming; // Guardamos el streaming de audio local.
    var mediaConnectionOtherPeer;
    var dataConnectionOtherPeer;
    // Pide al usuario permiso para usar un dispositivo multimedia como una cámara o 
    //micróono, la linea de abajo asegura que getUserMedia sea el corecto en chrome
    // y firefox.
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    //verificamos si el navegador soporta webRTC, y transmision de auido y video
    if (util.browser != 'Unsupported' && util.supports.audioVideo) {
        console.log("NAVEGADOR COMPATIBLE Y NAVEGAODR CAPAZ DE TRANSMITIR AUDIO Y VDEO");
        //conexion con servidor de peer
        peer = new Peer('user1', {
            key: 'peerjs',
            host: 'webrtc01.kubera.co',
            path: '/',
            port: 3000,
            config: {
                'iceServers': [{
                    url: 'stun:stun.l.google.com:19302',
                }, {
                    url: 'stun:stun.ideasip.com'
                }, {
                    url: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                }]
            },
            debug: 3,
            secure: true //indica si la conexon es con http (false) o https (true)
        });
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        //Se emite cuando se establece una conexión a PeerServer.Puede utilizar el peer 
        //antes de que se emita, pero los mensajes al servidor se pondrán en cola. 
        //Id es el ID de intermediación del peer (que fue proporcionado en el constructor 
        //o asignado por el servidor).
        peer.on('open', function(id) {
            console.log("CONEXION CON PEER SERVER EXITOSA BAJO EL IDENTIFICADOR --> " + id);
        });
        //Se emite cuando se establece una nueva conexión de datos desde un peer remoto.
        peer.on('connection', function(dataConnection) {
            console.log("NUEVA CONEXION ENTRANTE CONMIGO");
            //dataConnection es la conexion de datos con el otro peer, puedo enviar mensajes 
            // desde aqui o hacer otras cosas, posibiliades:
            //.send = dataConnection.send(data);
            // .close = dataConnection.close(); -> Cierra la conexión de datos correctamente, 
            //  limpiando DataChannels y PeerConnections subyacentes.
            // .on = dataConnection.on(event, callback); -> Establezca a los oyentes los eventos 
            //  de conexión de datos.
            //adicionalmente se puede conseguir la siguiente informacion:
            //.dataChannel = Una referencia al objeto RTCDataChannel asociado con la conexión.
            //
            //.label = La etiqueta opcional ingresada o asignada por PeerJS cuando se 
            // inició la conexión
            //
            //.metadata = Cualquier tipo de metadatos asociados con la conexión, pasados ​​por 
            //  quien inició la conexión.
            //
            //.open = Esto es true si la conexión está abierta y lista para lectura / escritura.
            //
            //.reliable = Si los canales de datos subyacentes son fiables; Cuando se inició la conexión.
            //
            //.serialization = El formato de serialización de los datos enviados a través de la conexión. 
            //Puede ser binary (predeterminado), binary-utf8, json o none.
            //
            //.type = Para las conexiones de datos, esto siempre es 'data'.
            //
            //.bufferSize = El número de mensajes en cola para enviar una vez que el búfer del 
            //  navegador ya no está lleno.
            //Se emite cuando se reciben datos del interlocutor remoto.
            dataConnection.on('data', function(data) {
                console.log("DATOS RECIBIDOS DESDE EL OTRO PEER ", data);
            });
            //Se emite cuando la conexión está establecida y lista para usar.
            dataConnection.on('open', function() {
                console.log("CONEXION CON OTRO PEER LISTA PARA UTILIZAR");
            });
            //Se emite cuando yo o el  peer remoto cierra la conexión de datos.
            dataConnection.on('close', function() {
                console.log("CONEXION CON OTRO PEER CERRADA");
            });
            // deteccion de errores en la otra conexion
            dataConnection.on('error', function(err) {
                console.log("ERROR EN LA CONEXION CON OTRO PEER ", err);
            });
        });
        //Se emite cuando un peer remoto intenta llamarle. El mediaConnection emitido 
        //aún no está activo; Primero debe contestar la llamada 
        //(mediaConnection.answer ([stream]);). 
        //A continuación, puede escuchar el evento de flujo.
        peer.on('call', function(mediaConnection) {
            //informacion que se puede sacar de esta mediaConnection
            // .open = Si la conexión de medios está activa (por ejemplo, 
            //  su llamada ha sido contestada). Puede comprobarlo si desea 
            //  establecer un tiempo de espera máximo para una llamada unilateral.
            //
            // .metadata = Cualquier tipo de metadatos asociados con la conexión,
            //   pasados ​​por quien inició la conexión.
            //
            // .peer = El ID del peer en el otro extremo de esta conexión.
            //
            // .type = Para las conexiones de medios, esto siempre es "media".
            console.log("NUEVA LLAMADA ENTRANTE");
            mediaConnection.answer(videoStreaming);
            //Se emite cuando un compañero remoto agrega un flujo.
            mediaConnection.on('stream', function(stream) {
                console.log("CAPTURAMOS STREAMING ENTRANTE");
            });
            //Se emite cuando ud o el peer remoto cierra la conexión de medios.
            mediaConnection.on('close', function() {
                console.log("YO O EL PEER REMOTO CERRAMOS LA CONEXION");
            });
            //Se emite cuando se captura algun error
            mediaConnection.on('error', function(err) {
                console.log("ERROR DECTADO EN EL MEDIA CONNECTION ", err);
            });
        });
        //Se emite cuando el peer se destruye y ya no puede aceptar o crear 
        //nuevas conexiones. En este momento, las conexiones de los compañeros 
        //estarán cerradas.
        peer.on('close', function() {
            console.log("PEER DESTRUIDO");
            peer.destroy();
        });
        //Se emite cuando el peer se desconecta del servidor de señalización, 
        //ya sea manualmente o porque se ha perdido la conexión con el servidor 
        //de señalización. Cuando un peer está desconectado, sus conexiones existentes 
        //permanecerán vivas, pero el peer no puede aceptar o crear nuevas conexiones. 
        //Puede volver a conectarse al servidor llamando a peer.reconnect ()
        peer.on('disconnected', function() {
            console.log("CONEXION CON PEER SERVER PERDIDA");
            peer.reconnect();
        });
        //Los errores en los peers son casi siempre fatales y destruirán a los peers.
        //Los errores del socket subyacente y PeerConnections se envían aquí.
        //posibles errores:
        //'browser-incompatible' = El navegador del cliente no admite algunas o todas 
        //  las funciones WebRTC que está intentando utilizar.
        //
        //'disconnected' = Ya ha desconectado este peer del servidor y ya no puede 
        //  realizar ninguna conexión nueva en él.
        //
        //'invalid-id' = El identificador pasado en el constructor Peer contiene 
        //  caracteres no válidos.
        //
        //'invalid-key' = La clave API pasada al constructor Peer contiene caracteres no 
        //  permitidos o no está en el sistema (sólo servidor de nube).
        //
        //'network' = Se ha perdido o no se puede establecer una conexión con el servidor 
        //  de señalización.
        //
        //'peer-unavailable' = El peer con el que está intentando conectarse no existe.
        //
        //'ssl-unavailable' = PeerJS se está utilizando de forma segura, pero el servidor de 
        //  la nube no es compatible con SSL. Utilice un PeerServer personalizado.
        //
        //'server-error' = No se ha podido alcanzar el servidor.
        //
        //'socket-error' = error con el socket
        //
        //'socket-closed' = socket cerrado
        //
        //'unavailable-id' = El identificador pasado en el constructor Peer ya está tomado.
        //
        //'webrtc' = errores con el WebRTC nativo
        peer.on('error', function(err) {
            console.log("ERROR RECIBIDO DEL TIPO " + err.type);
        });
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        // EVENTOS DE NOTICIACION QUE TIENEN QUE VER CON EL PEER SERVER
        //PARA CONECTARSE CON OTRO USUARIO 
        //Se conecta al peer remoto especificado por id y devuelve una conexión de datos. 
        //  Asegúrese de escuchar en el evento de error en caso de que la conexión falla.
        dataConnectionOtherPeer = peer.connect('user2', {
            metadata: {
                username: 'metadata, nformacion a pasar a otra persona'
            }
        });
        //Se emite cuando se reciben datos del interlocutor remoto.
        dataConnectionOtherPeer.on('data', function(data) {
            console.log("peer.connect/ DATOS RECIBIDOS DESDE EL OTRO PEER ", data);
        });
        //Se emite cuando la conexión está establecida y lista para usar.
        dataConnectionOtherPeer.on('open', function() {
            console.log("peer.connect/ CONEXION CON OTRO PEER LISTA PARA UTILIZAR");
            //PARA LLAMAR A OTRO USUARIO
            if (videoStreaming == undefined || videoStreaming == null) {
                var constraints = {
                    audio: false,
                    video: {
                        mandatory: {
                            maxWidth: 220,
                            maxHeight: 130,
                            minWidth: 220,
                            minHeight: 130
                        }
                    }
                };
                navigator.getUserMedia(constraints, function(localMediaStream) {
                    videoStreaming = localMediaStream;
                    console.log("STREAMING LOCAL", videoStreaming);
                    callOtherPeer(videoStreaming);
                }, function(err) {
                    // posibles errore:
                    // SecurityError = Permiso para usar un dispositivo fue denegado por el usuario o por el sistema.
                    // NotFoundError = No se encontraron pistas multimedia del tipo especificado que satisfagan las restricciones especificadas.
                    console.log("OCURRIO UN ERROR TOMANDO VIDEO LOCAL " + err);
                });
            } else {
                callOtherPeer(videoStreaming);
            }
            //PARA LLAMAR A OTRO USUARIO
        });
        //Se emite cuando yo o el  peer remoto cierra la conexión de datos.
        dataConnectionOtherPeer.on('close', function() {
            console.log("peer.connect/ CONEXION CON OTRO PEER CERRADA");
        });
        // deteccion de errores en la otra conexion
        dataConnectionOtherPeer.on('error', function(err) {
            console.log("peer.connect/ ERROR EN LA CONEXION CON OTRO PEER ", err);
        });
        //PARA CONECTARSE CON OTRO USUARIO 
        function callOtherPeer(localMediaStream) {
            mediaConnectionOtherPeer = peer.call('user2', localMediaStream, {
                metadata: {
                    type: 'Video'
                }
            });
            //Se emite cuando un compañero remoto agrega un flujo.
            mediaConnectionOtherPeer.on('stream', function(stream) {
                console.log("call/ CAPTURAMOS STREAMING ENTRANTE");
            });
            //Se emite cuando ud o el peer remoto cierra la conexión de medios.
            mediaConnectionOtherPeer.on('close', function() {
                console.log("call/ YO O EL PEER REMOTO CERRAMOS LA CONEXION");
                mediaConnectionOtherPeer.close();
            });
            //Se emite cuando se captura algun error
            mediaConnectionOtherPeer.on('error', function(err) {
                console.log("call/ ERROR DECTADO EN EL MEDIA CONNECTION ", err);
            });
        }
    } else {
        console.log("NAVEGADOR NO COMPATIBLE Y/O NAVEGAODR INCAPAZ DE TRANSMITIR AUDIO Y VDEO");
    }
});
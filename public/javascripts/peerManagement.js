$(document).ready(function() {
    var peer; // Donde se guarda la instancia de la conexion con peer
    var videoStreaming; // Guardamos el streaming de video local.
    var audioStreaming; // Guardamos el streaming de audio local.
    var videoMediaConnectionOtherPeer;
    var audioMediaConnectionOtherPeer;
    var videoStreamingOtherPeer;
    var audioStreamingOtherPeer;
    var dataConnectionOtherPeer;
    var videoStatus = false;
    var audioStatus = false;
    var peerServerConnection = false;
    var otherPeerConnection = false;
    var triyingVideoConnectionFlag = 0;
    var triyingAudioConnectionFlag = 0;
    var audioConnectInitFlag = 0;
    var videoConnectInitFlag = 0;
    var audioInterval, videoInterval, monitoringIntervalVideo, monitoringIntervalAudio;
    var monitoringIntervalFlagVideo = 0;
    var monitoringIntervalFlagAudio = 0;


    var videoWidth = 320;
    var videoHeigth = 240;
    var qualitySelected = "QVGA";

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    if (util.browser != 'Unsupported' && util.supports.audioVideo) {

    console.log("NAVEGADOR COMPATIBLE Y NAVEGAODR CAPAZ DE TRANSMITIR AUDIO Y VDEO");

        if (util.browser === 'Firefox'){

            peer = new Peer('user1', {
            key: 'peerjs',
            host: 'webrtc01.kubera.co',
            path: '/',
            port: 3000,
            config: {
                'iceServers': [{
                    urls: 'stun:stun.l.google.com:19302',
                }, {
                    urls: 'stun:stun.ideasip.com'
                }, {
                    urls: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                }]
            },
            debug: 3,
            secure: true //indica si la conexon es con http (false) o https (true)
        });

        }else{


        
        peer = new Peer('user1', {
            key: 'peerjs',
            host: 'webrtc01.kubera.co',
            path: '/',
            port: 3000,
            config: {
                'iceServers': [{
                    url: 'stun:stun.l.google.com:19302',
                }, {
                    url: 'stun:stun.ideasip.com'
                }, {
                    url: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                }]
            },
            debug: 3,
            secure: true //indica si la conexon es con http (false) o https (true)
        });

 }


        peer.on('open', function(id) {
            console.log("CONEXION CON PEER SERVER EXITOSA BAJO EL IDENTIFICADOR --> " + id);
            peerServerConnection = true;
        });
        peer.on('close', function() {
            console.log("PEER DESTRUIDO");
            peerServerConnection = false;
            peer.destroy();
        });
        peer.on('disconnected', function() {
            console.log("CONEXION CON PEER SERVER PERDIDA");
            peerServerConnection = false;
            peer.reconnect();
        });
        peer.on('call', function(mediaConnection) {
            console.log("NUEVA LLAMADA ENTRANTE");
            mediaConnection.answer(videoStreaming);
            mediaConnection.on('stream', function(stream) {
                console.log("CAPTURAMOS STREAMING ENTRANTE");
                if (mediaConnection.metadata.type === 'Video') {
                    videoInterval = setInterval(function(stream) {
                        console.log("ESTADO DEL VIDEO -> " + stream.active);
                        if (stream.active) {
                            sendPeerData('videoOk');
                            $('#videoUsr2').prop('src', URL.createObjectURL(stream));
                            videoStreamingOtherPeer = stream;
                        }
                        clearInterval(videoInterval);
                        if (monitoringIntervalFlagVideo == 0) {
                            monitoringIntervalFlagVideo = 1;
                            monitoringIntervalVideo = setInterval(monitoringMediaConnectionVideo, 2000);
                        }
                    }, 5000, stream);
                } else if (mediaConnection.metadata.type === 'Audio') {
                    audioInterval = setInterval(function(stream) {
                        console.log("ESTADO DEL AUDIO -> " + stream.active);
                        if (stream.active) {
                            sendPeerData('audioOk');
                            $('#audioUsr2').prop('src', URL.createObjectURL(stream));
                            audioStreamingOtherPeer = stream;
                        }
                        clearInterval(audioInterval);
                        if (monitoringIntervalFlagAudio == 0) {
                            monitoringIntervalFlagAudio = 1;
                            monitoringIntervalAudio = setInterval(monitoringMediaConnectionAudio, 2000);
                        }
                    }, 5000, stream);
                }
            });
            mediaConnection.on('close', function() {
                console.log("YO O EL PEER REMOTO CERRAMOS LA CONEXION", mediaConnection.peer);
            });
            mediaConnection.on('error', function(err) {
                console.log("ERROR DECTADO EN EL MEDIA CONNECTION ", err);
            });
        });

        function tryDataConnection() {
            dataConnectionOtherPeer = peer.connect('user2', {
                metadata: {
                    username: 'metadata, nformacion a pasar a otra persona'
                }
            });
            dataConnectionOtherPeer.on('data', function(data) {
                console.log("peer.connect/ DATOS RECIBIDOS DESDE EL OTRO PEER ", data);
                if (data == "videoOk") {
                    videoConnectInitFlag = 0;
                } else if (data == "audioOk") {
                    audioConnectInitFlag = 0;
                } else if (data == "retryVideo") {
                    triyingVideoConnectionFlag = 0;
                    stopLocalVideo();
                    videoConnectInitFlag = 0;
                    $("#videoSharing").trigger('click');
                } else if (data == "retryAudio") {
                    triyingAudioConnectionFlag = 0;
                    stopLocalAudio();
                    audioConnectInitFlag = 0;
                    $("#audioSharing").trigger('click');
                } else if (data == 'videoOut') {
                    triyingVideoConnectionFlag = 0;
                    videoConnectInitFlag = 0;
                    clearInterval(monitoringIntervalVideo);
                    monitoringIntervalFlagVideo = 0;
                    stopRemoteVideo();
                } else if (data == 'audioOut') {
                    triyingAudioConnectionFlag = 0;
                    audioConnectInitFlag = 0;
                    clearInterval(monitoringIntervalAudio);
                    monitoringIntervalFlagAudio = 0;
                    stopRemoteAudio();
                }
            });
            dataConnectionOtherPeer.on('open', function() {
                console.log("peer.connect/ CONEXION CON OTRO PEER LISTA PARA UTILIZAR");
                otherPeerConnection = true;
            });
            dataConnectionOtherPeer.on('close', function() {
                console.log("peer.connect/ CONEXION CON OTRO PEER CERRADA");
                otherPeerConnection = false;
                stopLocalVideo();
                stopRemoteVideo();
            });
            dataConnectionOtherPeer.on('error', function(err) {
                console.log("peer.connect/ ERROR EN LA CONEXION CON OTRO PEER ", err);
                otherPeerConnection = false;
                stopLocalVideo();
                stopRemoteVideo();
            });
        }
        peer.on('connection', function(dataConnection) {
            console.log("NUEVA CONEXION ENTRANTE CONMIGO");
            dataConnection.on('data', function(data) {
                console.log("DATOS RECIBIDOS DESDE EL OTRO PEER ", data);
                if (data == "videoOk") {
                    videoConnectInitFlag = 0;
                } else if (data == "audioOk") {
                    audioConnectInitFlag = 0;
                } else if (data == "retryVideo") {
                    triyingVideoConnectionFlag = 0;
                    stopLocalVideo();
                    videoConnectInitFlag = 0;
                    $("#videoSharing").trigger('click');
                } else if (data == "retryAudio") {
                    triyingAudioConnectionFlag = 0;
                    stopLocalAudio();
                    audioConnectInitFlag = 0;
                    $("#audioSharing").trigger('click');
                } else if (data == 'videoOut') {
                    triyingVideoConnectionFlag = 0;
                    videoConnectInitFlag = 0;
                    clearInterval(monitoringIntervalVideo);
                    monitoringIntervalFlagVideo = 0;
                    stopRemoteVideo();
                } else if (data == 'audioOut') {
                    triyingAudioConnectionFlag = 0;
                    audioConnectInitFlag = 0;
                    clearInterval(monitoringIntervalAudio);
                    monitoringIntervalFlagAudio = 0;
                    stopRemoteAudio();
                }
            });
            dataConnection.on('open', function() {
                console.log("CONEXION CON OTRO PEER LISTA PARA UTILIZAR");
                otherPeerConnection = true;
            });
            dataConnection.on('close', function() {
                console.log("CONEXION CON OTRO PEER CERRADA");
                otherPeerConnection = false;
                stopLocalVideo();
                stopRemoteVideo();
            });
            dataConnection.on('error', function(err) {
                console.log("ERROR EN LA CONEXION CON OTRO PEER ", err);
                otherPeerConnection = false;
                stopLocalVideo();
                stopRemoteVideo();
            });
        });
        $(document).on('click', '#videoSharing', function(event) {
            event.preventDefault();
            if (peerServerConnection == true && otherPeerConnection == true) {
                console.log("Iniciar compartir video");
                videoConnectInitFlag = 1;
                if (videoStatus) {
                    stopLocalVideo();
                    videoStatus = false;
                    triyingVideoConnectionFlag = 0;
                    videoConnectInitFlag = 0;
                    sendPeerData('videoOut');
                } else {
                    retreiveLocalVideo(function(response) {
                        console.log(response);
                        if (response.status == 'err') {
                            console.log("ERROR AL BUSCAR STREAMING DE VIDEO ", response.errorMsg);
                            stopLocalVideo();
                        } else if (response.status == 'ok') {
                            $('#videoSharing').text('Desactivate Cam');
                            $('#videoUsr1').prop('src', URL.createObjectURL(response.streaming));
                            videoCallOtherPeer(response.streaming);
                        }
                    });
                }
            } else {
                console.log("PEER SERVER CONNECTION IS UNAVALIABLE OR OTHER PEER CONNECTION IS UNAVALIABLE");
                console.log("INTENTANDO CONECATAR PEER");
                tryDataConnection();
            }
        });
        $(document).on('click', '#audioSharing', function(event) {
            event.preventDefault();
            if (peerServerConnection == true && otherPeerConnection == true) {
                console.log("Iniciar compartir audio");
                audioConnectInitFlag = 1;
                if (audioStatus) {
                    stopLocalAudio();
                    audioStatus = false;
                    triyingAudioConnectionFlag = 0;
                    audioConnectInitFlag = 0;
                    sendPeerData('audioOut');
                } else {
                    retreiveLocalAudio(function(response) {
                        console.log(response);
                        if (response.status == 'err') {
                            console.log("ERROR AL BUSCAR STREAMING DE AUDIO ", response.errorMsg);
                            stopLocalAudio();
                        } else if (response.status == 'ok') {
                            $('#audioSharing').text('Desactivate Mic');
                            audioCallOtherPeer(response.streaming);
                        }
                    });
                }
            } else {
                console.log("PEER SERVER CONNECTION IS UNAVALIABLE OR OTHER PEER CONNECTION IS UNAVALIABLE");
                console.log("INTENTANDO CONECATAR PEER");
                tryDataConnection();
            }
        });

        function retreiveLocalVideo(callback) {

            if (videoStreaming == undefined || videoStreaming == null) {
                var constraints = {
                    video: {
                        mandatory: {
                            maxWidth: videoWidth,
                            maxHeight: videoHeigth,
                            minWidth: videoWidth,
                            minHeight: videoHeigth
                        }
                    }
                };
                navigator.getUserMedia(constraints, function(localMediaStream) {
                    videoStreaming = localMediaStream;
                    console.log("STREAMING LOCAL", videoStreaming);
                    var result = [];
                    result['streaming'] = videoStreaming;
                    result['status'] = 'ok';
                    videoStatus = true;
                    callback(result);
                }, function(err) {
                    console.log("OCURRIO UN ERROR TOMANDO VIDEO LOCAL " + err);
                    var result = [];
                    result['status'] = 'err';
                    result['errorMsg'] = err;
                    videoStatus = false;
                    callback(result);
                });
            } else {
                if (videoStreaming.open) {
                    var result = [];
                    result['streaming'] = videoStreaming;
                    result['status'] = 'ok';
                    videoStatus = true;
                    callback(result);
                } else {
                    var result = [];
                    result['status'] = 'err';
                    result['errorMsg'] = 'streaming inactive';
                    videoStreaming.getTracks().forEach(track => track.stop());
                    $('#videoUsr1').prop('src', '');
                    $('#videoSharing').text('Activate Cam');
                    videoStreaming = undefined;
                    videoStatus = false;
                    callback(result);
                }
            }
        }

        function stopLocalVideo() {
            console.log("DETENIENDO VIDEO LOCAL");
            if (videoStreaming != undefined || videoStreaming != null) {
                videoStreaming.getVideoTracks()[0].stop();
                $('#videoUsr1').prop('src', '');
                $('#videoSharing').text('Activate Cam');
                videoStreaming = undefined;
                videoStatus = false;
            }
        }

        function stopRemoteVideo(retry) {
            console.log("DETENIENDO VIDEO COMPARTIDO");
            if (videoMediaConnectionOtherPeer != undefined || videoMediaConnectionOtherPeer != null) {
                videoMediaConnectionOtherPeer = undefined;
                if (retry != undefined && retry == true && otherPeerConnection == true) {
                    console.log("retry conection");
                    triyingVideoConnectionFlag = triyingVideoConnectionFlag + 1;
                    if (triyingVideoConnectionFlag >= 11) {
                        console.log("IMPOSIBLE LOGRAR CONEXION DE VIDEO");
                        triyingVideoConnectionFlag = 0;
                        videoConnectInitFlag = 0;
                    } else {
                        $("#videoSharing").trigger('click');
                    }
                }
            }
        }
        /////AUDIO FUNTIONS
        function retreiveLocalAudio(callback) {
            if (audioStreaming == undefined || audioStreaming == null) {
                var constraints = {
                    audio: true
                };
                navigator.getUserMedia(constraints, function(localMediaStream) {
                    audioStreaming = localMediaStream;
                    console.log("STREAMING LOCAL AUDIO ", audioStreaming);
                    var result = [];
                    result['streaming'] = audioStreaming;
                    result['status'] = 'ok';
                    audioStatus = true;
                    callback(result);
                }, function(err) {
                    console.log("OCURRIO UN ERROR TOMANDO AUDIO LOCAL " + err);
                    var result = [];
                    result['status'] = 'err';
                    result['errorMsg'] = err;
                    audioStatus = false;
                    callback(result);
                });
            } else {
                if (audioStreaming.open) {
                    var result = [];
                    result['streaming'] = audioStreaming;
                    result['status'] = 'ok';
                    audioStatus = true;
                    callback(result);
                } else {
                    var result = [];
                    result['status'] = 'err';
                    result['errorMsg'] = 'streaming inactive';
                    audioStreaming.getTracks().forEach(track => track.stop());
                    $('#audioUsr1').prop('src', '');
                    $('#audioSharing').text('Activate Mic');
                    audioStreaming = undefined;
                    audioStatus = false;
                    callback(result);
                }
            }
        }

        function stopLocalAudio() {
            console.log("DETENIENDO AUDIO LOCAL");
            if (audioStreaming != undefined || audioStreaming != null) {
                audioStreaming.getAudioTracks()[0].stop();
                $('#audioSharing').text('Activate Mic');
                audioStreaming = undefined;
                audioStatus = false;
            }
        }

        function stopRemoteAudio(retry) {
            console.log("DETENIENDO AUDIO COMPARTIDO");
            if (audioMediaConnectionOtherPeer != undefined || audioMediaConnectionOtherPeer != null) {
                audioMediaConnectionOtherPeer = undefined;
                if (retry != undefined && retry == true && otherPeerConnection == true) {
                    console.log("retry conection");
                    triyingAudioConnectionFlag = triyingAudioConnectionFlag + 1;
                    if (triyingAudioConnectionFlag >= 11) {
                        console.log("IMPOSIBLE LOGRAR CONEXION DE AUDIO");
                        triyingAudioConnectionFlag = 0;
                        audioConnectInitFlag = 0;
                    } else {
                        $("#audioSharing").trigger('click');
                    }
                }
            }
        }

        function videoCallOtherPeer(localMediaStream) {
            console.log("LLAMADA DE VIDEO <---></--->");
            videoMediaConnectionOtherPeer = peer.call('user2', localMediaStream, {
                metadata: {
                    type: 'Video'
                }
            });
            videoMediaConnectionOtherPeer.on('stream', function(stream) {
                console.log("call/ CAPTURAMOS STREAMING ENTRANTE");
            });
            videoMediaConnectionOtherPeer.on('close', function() {
                console.log("call/ YO O EL PEER REMOTO CERRAMOS LA CONEXION");
                if (videoConnectInitFlag == 1) {
                    stopRemoteVideo(true);
                } 
            });
            videoMediaConnectionOtherPeer.on('error', function(err) {
                console.log("call/ ERROR DETECTADO EN EL MEDIA CONNECTION ", err);
            });
        }

        function audioCallOtherPeer(localMediaStream) {
            console.log("LLAMADA DE AUDIO <---></--->");
            audioMediaConnectionOtherPeer = peer.call('user2', localMediaStream, {
                metadata: {
                    type: 'Audio'
                }
            });
            audioMediaConnectionOtherPeer.on('stream', function(stream) {
                console.log("callAudio/ CAPTURAMOS STREAMING ENTRANTE");
            });
            audioMediaConnectionOtherPeer.on('close', function() {
                console.log("callAudio/ YO O EL PEER REMOTO CERRAMOS LA CONEXION", audioMediaConnectionOtherPeer.peer);
                if (audioConnectInitFlag == 1) {
                    stopRemoteAudio(true);
                }
            });
            audioMediaConnectionOtherPeer.on('error', function(err) {
                console.log("callAudio/ ERROR DETECTADO EN EL MEDIA CONNECTION ", err);
            });
        }

        function sendPeerData(data) {
            var pointerSendMsg = 0;
            for (var currentPeerId in peer.connections) {
                if (!peer.connections.hasOwnProperty(currentPeerId)) {
                    return;
                }
                var connectionsWithCurrentPeer = peer.connections[currentPeerId];
                for (var i = 0; i < connectionsWithCurrentPeer.length; i++) {
                    if (connectionsWithCurrentPeer[i].type == 'data') {
                        if (connectionsWithCurrentPeer[i].open) {
                            connectionsWithCurrentPeer[i].send(data);
                            pointerSendMsg = 1;
                        } else {
                            console.log("CONEXION NOT AVALIABLE TO SEND INFO");
                        }
                    }
                }
            }
            if (pointerSendMsg == 0) {
                tryDataConnection();
            }
        }

        function monitoringMediaConnectionVideo() {
            if (videoStreamingOtherPeer != undefined && videoStreamingOtherPeer != null) {
                if (!videoStreamingOtherPeer.active) {
                    console.log("DETECTAMOS UN ERROR CON LA CONEXION DE VIDEO DEL OTRO PEER");
                    triyingVideoConnectionFlag = triyingVideoConnectionFlag + 1;
                    if (triyingVideoConnectionFlag >= 11) {
                        console.log("IMPOSIBLE LOGRAR CONEXION DE VIDEO");
                        triyingVideoConnectionFlag = 0;
                        videoConnectInitFlag = 0;
                        clearInterval(monitoringIntervalVideo);
                        monitoringIntervalFlagVideo = 0;
                        alert("IMPOSIBLE LOGRAR CONEXION DE VIDEO.");
                    } else {
                        console.log("INTENTO DE RECONEXION NUMERO " + triyingVideoConnectionFlag);
                        clearInterval(monitoringIntervalVideo);
                        sendPeerData('retryVideo');
                        monitoringIntervalFlagVideo = 0;
                    }
                }
            }
        }

        function monitoringMediaConnectionAudio() {
            if (audioStreamingOtherPeer != undefined && audioStreamingOtherPeer != null) {
                if (!audioStreamingOtherPeer.active) {
                    console.log("DETECTAMOS UN ERROR CON LA CONEXION DE AUDIO DEL OTRO PEER");
                    triyingAudioConnectionFlag = triyingAudioConnectionFlag + 1;
                    if (triyingAudioConnectionFlag >= 11) {
                        console.log("IMPOSIBLE LOGRAR CONEXION DE AUDIO");
                        triyingAudioConnectionFlag = 0;
                        audioConnectInitFlag = 0;
                        clearInterval(monitoringIntervalAudio);
                        monitoringIntervalFlagAudio = 0;
                        alert("IMPOSIBLE LOGRAR CONEXION DE AUDIO.");
                    } else {
                        console.log("INTENTO DE RECONEXION NUMERO " + triyingAudioConnectionFlag);
                        clearInterval(monitoringIntervalAudio);
                        sendPeerData('retryAudio');
                        monitoringIntervalFlagAudio = 0;
                    }
                }
            }
        }
        tryDataConnection();



// QUALITY SELECT LOGIC
$("#videoQuality").change(function() {

  var id = $(this).children(":selected").attr("id");
  console.log("QUALITY SELECTED --->>>" + id);

 

    if(id == "QVGA"){
   videoWidth = 320;
    videoHeigth = 240;
    qualitySelected = "QVGA";
    }else if(id == "VGA"){
   videoWidth = 640;
    videoHeigth = 480;
    qualitySelected = "VGA";
    }else if(id == "HD"){
   videoWidth = 1280;
    videoHeigth = 720;
    qualitySelected = "HD";
    }


    if (videoStreaming != undefined || videoStreaming != null) {

        stopLocalVideo();
        videoStatus = false;
        triyingVideoConnectionFlag = 0;
        videoConnectInitFlag = 0;
        sendPeerData('videoOut');

        $("#videoSharing").trigger('click');

    }


});

    } else {
        console.log("NAVEGADOR NO COMPATIBLE Y/O NAVEGADOR INCAPAZ DE TRANSMITIR AUDIO Y VDEO");
    }
});